import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController,LoadingController, AlertController } from 'ionic-angular';

import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';

import { User } from '../../models/user';
import { Course } from '../../models/course';

import { ConfigService } from '../../services/config';
import { CourseService } from '../../services/course';
import { UserService } from '../../services/users';
import { WishlistService } from '../../services/wishlist';
import { Storage } from '@ionic/storage';


import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'page-pdf',
  templateUrl: 'pdf.html'
})

export class PdfPage implements OnInit{
	
	pdf : any;
	a : any;
	constructor(
		
        private config: ConfigService,
  		private navParams: NavParams,
		private userService: UserService,
		public modalCtrl:ModalController,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
		public sanitizer: DomSanitizer,
  		) {}

	
	
	ngOnInit() {	
	 this.pdf  = this.navParams.data['pdf'];	 
	}	
	

	
	ionViewDidLoad(){	
			this.a = this.navParams.data['pdf'];
			
	}
	
	transform() {
		return this.sanitizer.bypassSecurityTrustResourceUrl(this.navParams.data['pdf']);
	}
}

