import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController,LoadingController, AlertController } from 'ionic-angular';

import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';


//import { ProfilePage } from '../profile/profile';

import { User } from '../../models/user';
import { Course } from '../../models/course';

import { ConfigService } from '../../services/config';
import { CourseService } from '../../services/course';
import { UserService } from '../../services/users';
import { WishlistService } from '../../services/wishlist';

import { FullCourse } from '../../models/course';

//import { Response } from '@angular/http';
import { Storage } from '@ionic/storage';
import * as WC from 'woocommerce-api';

//import { TransPage } from '../trans/trans';
//import * as midtransClient from 'midtrans-client';
import { OrderPage } from '../order/order';

import { SearchPage } from '../search/search';
import { CoursePage } from '../course/course';
import { ProfilePage } from '../profile/profile';

 
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html'
})

export class CheckoutPage implements OnInit{
	
	isLoggedIn: boolean = false;
	user: User;
	course: Course;
	
	WooCommerce: any;
	// private midTrans: any;
	private user_id: number;
	private baseUrl: string;
	
	userfirst: string;
	userlast: string;
	useremail: string;
	userphone: string;
	usercoupon: string;
	
	useCoupon: boolean = false;
	couponItem: any;
	couponDisc: number=0;
	
	product: any;
	customer: any;
	random_number: number;
	
	//private observable:Observable<any>;
	private http: Http;
	//MTUrl:'https://api.sandbox.midtrans.com';
	
	constructor(
        private config: ConfigService,
  		private navParams: NavParams,
		private userService: UserService,
		public modalCtrl:ModalController,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
				  		  
		public navCtrl: NavController,

		) {
		
		this.baseUrl = this.config.settings.url;
		//this.baseUrl = 'https://staging-arkademi.kinsta.cloud/';
		this.WooCommerce = WC({
		url: this.baseUrl,
		// api key staging woocommerce
		//consumerKey: "ck_862bc05d08f777af2e024d9b13ff54c2f56b9b5d",
		//consumerSecret: "cs_3c476e60b7aeccf492905a01cdf25b6da1be9912",
		//di bawah ini api key ke ????
		//consumerKey: "ck_52f450964c8515c0bd2d7a83fde2ac820ee51384",
		//consumerSecret: "cs_ecdfa2006cc03f8cb571317009f5f212766d85ec",
		// di bawah ini api key ke Live
		consumerKey: "ck_f07aa7a4a60e6b80ac846bbbc16b073f02e50e76",
		consumerSecret: "cs_4f0316d0a5ac11b7788670a0a8ce1a53ab3bf85f",
		wpAPI: true, // Enable the WP REST API integration
		queryStringAuth: true,
		verifySsl: true,
		version: 'wc/v3' // WooCommerce WP REST API version
		});
				
	}

	ngOnInit() {
		// Nyoba bikin halaman baru nyambung ama WooCommerce
		let loading = this.loadingCtrl.create({
            content: '<img src="assets/images/bubbles.svg">',
            duration: 15000,//this.config.get_translation('loadingresults'),
            spinner:'hide',
            showBackdrop:true,

        });
		loading.present();
		this.course = this.navParams.data['course'];
		for (let x in this.course)
		{
			console.log(x+':'+this.course[x]);
		}
		
		this.user=this.config.user;
		let id_confirmed:string;
		let get_id:any =this.course.price_html[0];
		let pos1 = get_id.link.search('=');
		let pos2 = get_id.link.length;
		if (pos1 >=0) {
			id_confirmed = get_id.link.substr(pos1+1,pos2-pos1);
			console.log(id_confirmed);
		}
		//Load Product
		this.WooCommerce.getAsync('products/'+id_confirmed).then((data) => {
			let res = (JSON.parse(data.body));
			if (res) {
				this.product = res;
				this.random_number = (this.getRandomNumber());
				//this.random_number = (this.getRandomNumber() -this.getDiscount(10));
				loading.dismiss();
			} else {
				console.log('bendanya gak ada');
				loading.dismiss();
			}
		});
		// Load Customer
		this.WooCommerce.getAsync('customers/'+this.config.user.id).then((data) => {
			let res = (JSON.parse(data.body));
			if (res) {
				this.customer = res;
				this.userfirst= this.customer.first_name;
				this.userlast= this.customer.last_name;
				this.useremail= this.customer.email;
			} else {
				console.log('orangnya gak ada');
			}
		});
		//this.random_number = this.getRandomNumber();
	}

    openSearch(){
        let modal = this.modalCtrl.create(SearchPage);
        modal.present();
    }
    backToCourse(){
      //this.navCtrl.push(CoursePage,{'id':this.coursestatus.course_id});
    } 
	
	showAlert(message:string=null,message2:string=null) {
		let alert = this.alertCtrl.create({
			title:message,
			subTitle:message2,
			buttons:['Ok']
		});
		alert.present();
	}
	getRandomNumber() {
		return Math.floor(-((Math.random()*899)+100));
	}
	getDiscount(percentage) {
		return Math.round(this.product.price*(percentage/100));
	}
	
	getTotalPrice() {
		let totalPrice = Math.floor(parseInt(this.course.price) + this.random_number + this.couponDisc);
		if (totalPrice <= 0) {
			totalPrice = 0;
		}
		return totalPrice; 
	}
	testOrderItem(){
		let loading = this.loadingCtrl.create({
            content: '<img src="assets/images/bubbles.svg">',
            duration: 15000,//this.config.get_translation('loadingresults'),
            spinner:'hide',
            showBackdrop:true,

        });
		loading.present();
		
		this.WooCommerce.getAsync('orders/4863').then((data) => {
				let res = (JSON.parse(data.body));
				if (res) {
					for (let x in res)
					{
						console.log(x+':'+res[x]);
					}
					//console.log(JSON.stringify(res));
					console.log('saved');
					this.navCtrl.push(OrderPage,{'order': res});
					//let modal = this.modalCtrl.create(OrderPage,{'order': res});
					//modal.present();
					//loading.dismiss();

					//Ke halaman berikutnya
				} else {
					//return false
					console.log('Kenapa error ya?');
				}
			});
			console.log('Selesai');
	}
	orderItem(){
		if (this.userfirst && this.userlast && this.useremail && this.userphone) {
			let totalPriceEffect = "on-hold";
			if (this.getTotalPrice() <= 0 ) {
				totalPriceEffect = "completed";
			}
			// Buat template order
			let item = {
				customer_id:this.user.id,
				status:totalPriceEffect,
				billing:{
					first_name:this.userfirst,
					last_name:this.userlast,
					email:this.useremail,
					phone:this.userphone,
				},
				payment_method:'bacs',
				payment_method_title:"Transfer ke Mandiri (app)",
				line_items: [
					{
						name:this.product.name,
						product_id:this.product.id,
						quantity:1,
						subtotal:this.product.price,
					}
				],
				fee_lines: [
					{
						name:"Diskon",
						tax_status:"taxable",
						amount:this.random_number.toString(),
						total:this.random_number.toString(),
					}
				],
				coupon_lines:[],
			};
			console.log('Angka Random :'+this.random_number);
			if(this.useCoupon) {
				item.coupon_lines.push(this.couponItem);
			}

			//Masukin ke WooCommerce
			this.WooCommerce.postAsync('orders',item).then((data) => {
				let res = (JSON.parse(data.body));
				if (res) {
					for (let x in res)
					{
						console.log(x+':'+res[x]);
					}
					console.log(JSON.stringify(res));
					console.log('saved');
					this.navCtrl.push(OrderPage,{'order': res});
					//let modal = this.modalCtrl.create(OrderPage,{'order': res});
					//modal.present();
					//Ke halaman berikutnya
				} else {
					//return false
					console.log('Kenapa error ya?');
				}
			});
			console.log('Selesai');
		} else {
			this.showAlert('Data','Isi data anda dengan lengkap');
		}
	}
	
	// is the user already have the same order?
	checkOrders(user_id) {
		console.log("ID:"+user_id);
		this.WooCommerce.getAsync('orders?customer='+user_id).then((data) => {
			let res = (JSON.parse(data.body));
			if (res) {
				for (let x in res)
				{
					console.log(x+':'+res[x]);
				}
				console.log('Ada');
				return true
			} else {
				console.log('Gak ada');
				return false
			}
		});
	}
	removeCoupon(){
		this.couponItem = null;
		this.couponDisc = 0;
		this.useCoupon = false;
		if (this.random_number == 0) {
			this.random_number = (this.getRandomNumber());			
		}
	}
	checkCoupon(kupon){
		if (kupon) {
			let loading = this.loadingCtrl.create({
            content: '<img src="assets/images/bubbles.svg">',
            duration: 15000,//this.config.get_translation('loadingresults'),
            spinner:'hide',
            showBackdrop:true,
			});
			loading.present();			
		this.WooCommerce.getAsync('coupons?code='+kupon).then((data) => {
			let res = (JSON.parse(data.body));
			if (res.length) {
				let aw = res[0]
				for (let x in aw)
				{
					console.log(x+':'+aw[x]);
				}
				// Start checking
				let allTrue = true;
				// Check expire
				if ((allTrue) && (res[0].date_expires))	{
					let thisDay = (new Date()).toISOString();
					if (thisDay > res[0].date_expires) {
						this.couponItem = null;
						this.couponDisc = 0;
						loading.dismiss();
						this.showAlert('Kupon sudah tidak berlaku');
						this.useCoupon = false;
						allTrue= false;
					}
				}				
				// Check apa user pernah memakai kupon ini dan dibatasi?
				if ((allTrue) && (res[0].used_by.length > 0) && (res[0].usage_limit_per_user)) {
					let users = res[0].used_by;
					let check = true;
					for (let x in users)	{
						console.log(x+':'+users[x]);
						if (this.config.user.id == users[x]) {
							check = false;
						}
					}
					if (!check)
					{
						this.couponItem = null;
						this.couponDisc = 0;
						loading.dismiss();
						this.showAlert('Anda sudah menggunakan kupon ini');
						this.useCoupon = false;
						allTrue= false;						
					}
				}
				// Check produk yang berlaku
				if ((allTrue) && (res[0].product_ids.length > 0)) {
					let products = res[0].product_ids;
					let check = false;
					for (let x in products)	{
						console.log(x+':'+products[x]);
						if (this.product.id == products[x]) {
							check = true;
						}
					}
					if (!check)
					{
						this.couponItem = null;
						this.couponDisc = 0;
						loading.dismiss();
						this.showAlert('Kupon tak bisa dipakai di sini');
						this.useCoupon = false;
						allTrue= false;						
					}
				}
				// Check Maksimum harga
				if ((allTrue) && (res[0].maximum_amount >0)) {
					if (res[0].maximum_amount < parseInt(this.product.price))
					{
						console.log(res[0].maximum_amount +'<'+ this.product.price);
						this.couponItem = null;
						this.couponDisc = 0;
						loading.dismiss();
						this.showAlert('Harga kelas melebihi ketentuan');
						this.useCoupon = false;
						allTrue= false;
					}
				}
				// Check limit
				if ((allTrue) && (res[0].usage_limit)) {
					if (res[0].usage_count > res[0].usage_limit)
					{
						this.couponItem = null;
						this.couponDisc = 0;
						loading.dismiss();
						this.showAlert('Kupon sudah habis');
						this.useCoupon = false;
						allTrue= false;
					}
				}
				//Lewat semua test
				if (allTrue){
					if (res[0].discount_type == 'percent') {
						if(res[0].amount == 100){
						this.random_number = 0;	
						this.couponDisc = (((res[0].amount/100) * this.course.price));
						}else{
						this.couponDisc = (((res[0].amount/100) * this.course.price));	
						}
					} else {
						this.couponDisc = res[0].amount;
					}
					this.couponItem =
						{
							code:res[0].code,
							discount:this.couponDisc.toString(),
						};
					this.couponDisc = -this.couponDisc;
					this.useCoupon = true;
					loading.dismiss();
				}
			} else {
				console.log('kuponnya salah');
				this.couponItem = null;
				this.couponDisc = 0;
				loading.dismiss();
				this.showAlert('Kode Kupon Salah');
				this.useCoupon = false;
			}
		});
		} else {
			this.couponItem = null;
			console.log('kuponnya salah');
			this.couponDisc = 0;
			this.showAlert('Isi kode kupon');
			this.useCoupon = false;
		}
	}

}

