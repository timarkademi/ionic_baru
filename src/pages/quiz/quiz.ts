import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController,LoadingController } from 'ionic-angular';
//import { QuizPage } from '../quiz/quiz';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html'
})

export class QuizPage{
	
		
	constructor(
		public navCtrl: NavController,
  		private navParams: NavParams,
		public modalCtrl:ModalController,
		private loadingCtrl: LoadingController,	
	){}
	
	
	
	back(){
		
		//let modal = this.modalCtrl.create(HomePage);
        //modal.present();
		this.navCtrl.push(HomePage);
	}	
}

