import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController,LoadingController,AlertController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-password',
  templateUrl: 'lupapassword.html'
})

export class forgotPasswordPage{
	
	email ="";	
	//data : any;
	constructor(
		public navCtrl: NavController,
  		private navParams: NavParams,
		public modalCtrl:ModalController,
		private http:Http,
		private alertCtrl:AlertController,
		private loadingCtrl: LoadingController,	
	){}
	
	
	
	back(){
		
		this.navCtrl.push(ProfilePage);
	}	
	
	
	sendEmail(){
		
		
		if(this.email == ""){
			
					let alert = this.alertCtrl.create({
                        title: 'Email Tidak Valid ',
                        buttons: [' OK ']
                        });
                        alert.present();
			
		}else{
			let body = {email : this.email};
			
			//window.open('mailto:'+this.email+'?subject=Reset%20Password&body=https://arkademi.com/wp-login.php?action=lostpassword','_system');
			
			let type = "application/json; charset=UTF-8";
			let header = "new Headers({'Content-type':type})";
			let options = "new RequestOptions({headers:headers})";
			
			console.log(body);
			
			this.http.post("http://baznaspayakumbuh.com/email.php",
				JSON.stringify(body),options).map(res=>res.json()).subscribe((data) =>{
					if(data.success ==true){
						console.log(data);	
						let alert = this.alertCtrl.create({
                        title: 'Email Sent ',
                        buttons: [' OK ']
                        });
                        alert.present();
						
						
					}else{
						console.log(data);	
						
						let alert = this.alertCtrl.create({
                        title: 'Email Error ',
                        buttons: [' OK ']
                        });
                        alert.present();
						
						
					}	
				});
		
		}

	}	
	
}

