import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Storage } from '@ionic/storage';

@Injectable()
export class ConfigService{  
	
	loading:boolean;
	timestamp:number;
	lastaccess: number; //Last access datetime with website
	fetchedResources:any;
	user:any;
	isLoggedIn:boolean=false;

	homePage:any;

	baseUrl:string;
	oAuthUrl:string;
	
	wooUrl:string;

	lastCourse:any;
	environment:any;
	settings:any;

	defaultMenu:any;
	per_view:number=10;
	translations: any;
	directoryFilters:any;

	/*
		IMPORTANT DO NOT TOUCH
	*/
	defaultTrack:any;
	track:any;
	trackSync:any;
	contactinfo:any;
	terms_conditions:any;
	unread_notifications_count:number=0;
	wallet:any=[];
	per_page_comment=10;
	/*== END == */

	constructor(
		private storage:Storage,
		private http:Http)
	{

		this.loading=true;
		this.timestamp =  Math.floor(new Date().getTime() / 1000);
		this.environment ={
			'cacheDuration':86400,
			//'cacheDuration':0,
		};

		this.lastaccess = 0;
		this.storage.get('lastccess').then(res=>{
			if(res){
				this.lastaccess = res;	
			}			
		});

		this.per_view = 5;
		this.settings ={
			'version':1,
			//'url':'http://localhost/wplms/',
			//'client_id':'J2Ird68cl0R5cNoTmg2bv0f',
			'url':'https://arkademi.com/',
			//'url':'https://staging-arkademi.kinsta.cloud/',
			//'client_id':'OSBE73Zm2uNQMai0E4NNTaD',
			'client_id':'MR5rULcfmi5qj6iV37uiUpW',
			'client_secret':'', //Fetched from API call
			'state':'', // FETCHED from Site
			'access_token':'', // FETCHED on Login 
			

			'registration':'app',//'app' or 'site' or false
			'login':'app',//Select from 'app' or 'site' or false
			'facebook':{
				'enable':true
			},
			'google':{
				'enable':true,
			},
			'per_view':5,
			'force_mark_all_questions':false,
			'wallet':false,					// <<----------REQUIRES WPLMS version 3.4
			'inappbrowser_purchases':true, // <<----------REQUIRES WPLMS version 3.4
			'rtl':false,
			'units_in_inappbrowser':false,
		};

		this.baseUrl = this.settings.url+'wp-json/wplms/v1/';
		this.oAuthUrl = this.settings.url+'wplmsoauth/';
		
		this.wooUrl = this.settings.url+'wp-json/wc/v3/'; // salah, ternyata gak perlu :p

		this.defaultMenu = {
			'home': 'Home',
			'about':'About',
			'courses':'Courses',
			'instructors':'Instructors',
			'contact':'Contact'
		};
		
		this.homePage = {
			'featuredCourseSlider':1,
			'categories':1,
			'popular':1,
			'featured':1,
			'testcategory':1,
		};

		this.directoryFilters = {
			'categories':1,
			'instructors':1,
			'locations':1,
			'levels':1,
			'free_paid':1,
			'online_offline':0,
			'start_date':0,
		};


		/* WALLET RECHARGE : in APP PRODUCT IDS */
		
		this.wallet = [
			{'product_id':'wplms_r_50','points':50},
			{'product_id':'sample','points':50},
		];


		/* TRACKS LOADED COMPONENTS 
			STATUS : 
				0 NOT LOADED
				1 LOADED
				In array : Loaded
		*/
		this.defaultTrack = {
			'version'				:1,
			'counter'				:0,
			'user'					:0,
			'featured'				:0,// Check if there is any change in featured courses 
			'popular'				:0,// Check if there is any change in popular courses 
			'allcoursecategories'	:0,
			'allcourselocations'	:0,
			'allcourselevels'		:0,
			'allcourses'			:0,
			'allposts'				:0,
			'transactions'			:0,
			'posts'					:[],
			'dashboardCharts'       :[],
			'courses'				:[], // if loaded it exists here
			'stats'  				:0, // Check if any need to reload student statistics
			'notifications'			:0, // Check if any new notifications are added.
			'announcements'			:0, // Check if any new announcements are added for user
			'allinstructors'		:0,//track if new instructor is added in site
			'instructors'			:[], //if loaded it exists here
			'profile'				:0,
			'profiletabs'			:[],//if loaded it exists here
			'reviews'				:[],
			'course_status'			:[], //load course curriclum & statuses
			'statusitems'			:[], 
			'saved_results'			:[],
		};
		this.track = this.defaultTrack;
		this.unread_notifications_count=0;

		this.translations = {

			'home_title':'DEPAN',
			'home_subtitle':'Kelas Pilihan',
			'start_course': 'Mulai Kelas',
			'search_title':'Mencari...',
			'continue_course': 'Lanjutkan',
			'completed_course': 'Terselesaikan',
			'expired_course': 'Kadaluwarsa',
			'evaluation_course':'Dalam Evaluasi',
			'no_reviews':'Belum ada ulasan untuk kelas ini',
			'year': 'tahun',
			'years': 'tahun',
			'month': 'bulan',
			'months': 'bulan',
			'week':'minggu',
			'weeks':'minggu',
			'day':'hari',
			'days':'hari',
			'hour':'jam',
			'hours':'jam',
			'minute':'menit',
			'minutes':'menit',
			'second':'detik',
			'seconds':'detik',
			'expired':'kadaluwarsa',
			'completed':'terselesaikan',
			'start_quiz':'Mulai Kuis',
			'save_quiz':'Simpan Kuis',
			'submit_quiz':'Kirim Kuis',
			'marks': 'Skor',
			'marks_obtained':'Skor Diperoleh',
			'max_marks':'Skor Maksimal',
			'true':'Benar',
			'false':'Salah',
			'checkanswer':'Periksa Jawaban',

			'score':'Skor',
			'progress': 'PROGRES',
			'time': 'WAKTU',
			'filter_options':'SARING',
			'sort_options':'URUTKAN',
			'popular':'Populer',
			'recent':'Terbaru',
			'alphabetical':'Alfabet',
			'rated':'Rating Tertinggi',
			'start_date':'Segera Buka',
			'okay':'OKE',
			'dismiss':'BATAL',
			'select_category':'Pilih Kategori',
			'select_location':'Pilih Lokasi',
			'select_level':'Pilih Level',
			'select_instructor':'Pilih Mentor',
			'free_paid':'Pilih Harga Kelas',
			'price':'Harga',
			'apply_filters':'Saring',
			'close':'Tutup',
			'not_found':'Mencari kelas...',
			'no_courses':'Mencari kelas..',
			'course_directory_title':'Semua Kelas',
			'course_directory_sub_title':'Direktori Kelas',
			'all':'Semua',
			'all_free':'Gratis',
			'all_paid':'Berbayar',
			'select_online_offline':'Pilih Tipe Kelas',
			'online':'Online',
			'offline':'Offline',
			'after_start_date':'Dimulai setelah tanggal',
			'before_start_date':'Dimulai sebelum tanggal',
			'instructors_page_title':'Semua Mentor',
			'instructors_page_description':'Direktori Mentor',
			'no_instructors':'Tak ada mentor ditemukan',

			'get_all_course_by_instructor':'Kelas Lain Mentor',
			'profile':'Profil',
			'about':'Tentang',
			'courses':'Kelas',
			'marked_answer':'Jawaban Anda',
			'correct_answer':'Jawaban yang benar',
			'explanation': 'Penjelasan',
			'awaiting_results':'Memproses hasil kuis',
			'quiz_results':'Hasil Kuis',
			'retake_quiz':'Ikuti ulang kuis',
			'quiz_start':'Kuis dimulai',
			'quiz_start_content':'Anda memulai kuis',
			'quiz_submit':'Kuis terkirim',
			'quiz_submit_content':'Anda mengirimkan kuis',
			'quiz_evaluate':'Kuis terevaluasi',
			'quiz_evaluate_content':'Kuis terevaluasi',
			'certificate':'Sertifikat',
			'badge':'Lencana',
			'no_notification_found':'Tak ada notifikasi!',
			'no_activity_found' :' Tak ada aktivitas!',
			'back_to_course':'Kembali ke kelas',
			'review_course':'Ulas kelas',
			'finish_course':'Selesaikan Kelas',
			'login_heading':'Login',
			'login_title':'Selamat Datang',
			'login_content':'Kelas anda akan tersedia di semua perangkat',
			'login_have_account':'Sudah punya akun?',
			'login_signin':'Masuk',
			'login_signup':'Daftar',
			'login_terms_conditions':'Ketentuan Layanan',
			'signin_username':'Username atau Email',
			'signin_password':'Password',
			'signup_username':'Username',
			'signup_email':'Email',
			'signup_password':'Password',

			'signup':'Daftar',
			'login_back':'Kembali ke Login',
			'post_review':'Ulas kelas ini',
			'review_title':'Judul ulasan',
			'review_rating': 'Beri rating kelas ini',
			'review_content': 'Isi ulasan',
			'submit_review': 'Kirim ulasan',
			'rating1star':'Buruk',
			'rating2star':'Kurang',
			'rating3star':'Puas',
			'rating4star':'Istimewa',
			'rating5star':'Super!',
			'failed':'Gagal',
			'user_started_course':'Anda telah memulai kuis',
			'user_submitted_quiz':'Anda telah mengirim kuis',
			'user_quiz_evaluated':'Kuis terevaluasi',
			'course_incomplete':'Kelas belum selesai',
			'finish_this_course':'Selesaikan semua seri di kelas ini',
			'ok':'OK',
			'update_title':'Update',
			'update_read':'Baca',
			'update_unread':'Belum dibaca',
			'no_updates':'Tidak ada update',
			'wishlist_title': 'Wishlist',
			'no_wishlist':'Belum ada kelas wishlist',
			'no_finished_courses':'Belum ada kelas terselesaikan!',
			'no_results':'Belum ada nilai kelas!',
			'loadingresults':'Mohon tunggu...',
			'signup_with_email_button_label':'Daftar',
			'clear_cache':'Bersihkan cache',
			'cache_cleared':'Cache berhasil dibersihkan',
			'sync_data':'Sinkronkan Data',
			'data_synced':'Data tersinkron',
			'logout':'Keluar',
			'loggedout':'Anda berhasil keluar',

			'register_account':'Masuk atau daftar untuk melanjutkan',
			'email_certificates':'Kirim sertifikat ke email',
			'manage_data':'Kelola Data Tersimpan',
			'saved_information':'Informasi Tersimpan',
			'client_id':'Site Key',
			'saved_quiz_results':'Nilai tersimpan','timeout':'Waktu Habis',
			'app_quiz_results':'Hasil',
			'change_profile_image':'Ubah Gambar Profil',
			'pick_gallery':'Atur gambar dari galeri',
			'take_photo':'Ambil Foto',
			'cancel':'Batal',
			'blog_page':'Blog',
			'course_chart':'Statistik Kelas',
			'quiz_chart':'Statistik Kuis',
			'percentage':'Prosentase',
			'scores':'Skor',
			'edit':'Edit',
			'change':'Ubah',
			'edit_profile_field':'Edit Info Profil',
			'pull_to_refresh':'Tarik untuk Refresh',
			'refreshing':'merefresh...',
			'contact_page':'Kontak',
			'contact_name':'Nama',
			'contact_email':'Email',
			'contact_message':'Pesan',
			'contact_follow_us':'Ikuti Kami',
			'invalid_url':'URL salah',
			'read_notifications':'Baca notifikasi',
			'unread_notifications':'Notifikasi Baru',
			'logout_from_device':'Anda yakin ingin keluar?',
			'accept_continue':'Terima dan Lanjutkan',
			'finished':'Selesai',
			'active':'Aktif',
			'open_results_on_site':'Silakan periksa hasil kuis ini lewat website',
			'show_more':'selanjutnya',

			'show_less':'less',

			'buy':'Beli',
			'pricing_options':'Pilihan harga',
			'pricing_options_title':'Pilihan harga (geser untuk memilih)',
			'home_menu_title':'Depan',
			'directory_menu_title':'Direktori',
			'instructors_menu_title':'Mentor',
			'blog_menu_title':'Blog',
			'contact_menu_title':'Kontak',
			'quiz_menu_title':'Kuis',
			'popular_courses_title_home_page':'Kelas Populer',
			'popular_courses_subtitle_home_page':'Kelas Populer dan Trending',
			'categories_title_home_page':'Kategori',
			'categories_subtitle_home_page':'Lihat kelas lewat kategori',
			'directory_search_placeholder':'Cari',
			'featured_courses':'Kelas Pilihan',
			'selected_courses':'Kelas Pilihan',
			'markallquestions':'Mohon menjawab semua pertanyaan.',

			'credit':'Kredit',
			'debit':'Debit',
			'wallet_no_products':'Hubungi kami untuk produk Dompet!',
			'wallet_no_transactions': 'Tidak ada transaksi!',
			'pay_from_wallet': 'Bayar dari Dompet',
			'use_wallet':'Gunakan saldo di Dompet untuk membayar',
			'pay':'BAYAR',
			'login_to_buy':'Silakan Login untuk bergabung ke kelas ini',
			'login_again':'Mohon login kembali untuk membayar kelas ini',
			'insufficient_funds':'Saldo di Dompet tidak cukup. Tambah saldo anda.',
			'buy_from_site': 'Bayar melalui Website',
			'description':'deskripsi',
			'curriculum':'kurikulum',
			'reviews':'ulasan',
			'instructors':'mentor',
			'retakes_remaining':'Pengulangan Tersisa',
			'open_in_new_window':'Buka di jendela baru'
			};

		this.contactinfo={
			'title':'HUBUNGI KAMI',
			'message':'ARKADEMI<br>(PT Arkademi Daya Indonesia)<br>Khafi Residence B-2. Jl. Batu No. 48, Srengseng Sawah, Jagakarsa, Jakarta Selatan.<br>DKI Jakarta, Indonesia 12630<br>Email: halo@arkademi.com<br>Mobile/WA: 081520468081<br>',
			
			
			'facebook':'<a href="https://www.facebook.com/arkademiedu/">ArkademiEdu</a>',
		};
		this.terms_conditions = 'Dengan menggunakan layanan kami, Anda menerima Syarat dan Ketentuan Layanan Arkademi. Semua konten di Arkademi tidak untuk didistribusikan di luar Arkademi.';
	}

	get_translation(key:string){
		if(this.translations[key]){
			return this.translations[key];
		}
	}

	trackComponents(key:string){
		return this.track[key];
	}

	updateComponents(key,value){
		if(Array.isArray(this.track[key]) ){
			this.track[key].push(value);
			this.storage.set('track',this.track);
		}else{
			this.track[key]=value;
			this.storage.set('track',this.track);
		}
	}

	//Only for arrays
	removeFromTracker(key,value){
		if(Array.isArray(this.track[key])){
			if(this.track[key].length){
				if(this.track[key].indexOf(value) != -1){
					let k = this.track[key].indexOf(value);
					this.track[key].splice(k,1);
					this.storage.set('track',this.track);
				}
			}
		}
	}
	addToTracker(key,value){
		if(Array.isArray(this.track[key]) ){
			if(this.track[key].indexOf(value) == -1){
				console.log('in add to tracker array');
				this.track[key].push(value);
			}
		}else{
			console.log('in add to tracker single value');
			this.track[key] = value;
		}
		console.log(this.track);
		this.storage.set('track',this.track);
	}

	public set_settings(key,value){
		this.settings[key]=value;
		this.storage.set('settings',this.settings);
	}
	save_settings(){
		this.storage.set('settings',this.settings);
	}

	initialize(){
		this.storage.get('track').then(res=>{
			if(res){
				this.track = res;
				
			}
			this.getTracker();
		});

		this.storage.get('settings').then(res=>{
			if(res){
				this.settings = res;
			}
		});
		
		this.storage.get('user').then(res=>{
			if(res){
				this.user = res;
				if('id' in this.user){
					this.isLoggedIn = true;
					this.getTracker();
				}
			}
		});

		this.storage.get('lastcourse').then((d) => {
            this.lastCourse = d;
        });
	}

	isLoading(){
		return this.storage.get('track');
	}

	updateUser(){
		this.storage.get('user').then(res=>{
			if(res){
				this.user = res;
				if('id' in this.user){
					this.isLoggedIn = true;
				}
			}else{
				this.isLoggedIn = false;
				this.user = 0;
				this.storage.remove('user');
			}
		});
	}
	getLastCourse(){
		this.storage.get('lastcourse').then((d) => {
            this.lastCourse = d;
        });
	}
	matchObj(big:any,small:any){

		for(let i=0;i<big.length;i++){
			if(big[i].time == small.time && big[i].content == small.content){
				return true;
			}
		}
		return false;
	}

	getTracker(){
		
		console.log('FETCH STORED TRACKER');
		console.log(this.track);

		if(this.isLoggedIn){

			console.log('TRACKING ->'+this.isLoggedIn+' = '+this.user.id);
			console.log(this.user);

			this.http.get(`${this.baseUrl}track/`+this.user.id+`?access=`+this.lastaccess)
            .map(response =>{
            	return response.json();
            }).subscribe(res=>{
            	if(res){
            		console.log('Version compare : '+res.version+' == '+this.track.version);
            		if(res.version != this.track.version){
            			//Re-record all cached data.
            			this.defaultTrack.version  = res.version;
            			this.track = this.defaultTrack;
            		}else{
            			
            			var keys = Object.keys(res);
            			console.log('keys');
            			console.log(keys);
            			if(keys.length){
	            			for(let i=0;i<keys.length;i++){
	            				if(keys[i] in this.track){
	            					if(Array.isArray(this.track[keys[i]]) && Array.isArray(res[keys[i]])){
	            						let missing = this.track[keys[i]].filter(item => res[keys[i]].indexOf(item) < 0);
	            						
	            						this.track[keys[i]] = missing;

	            					}else{
	            						if(res[keys[i]] && keys[i] != 'version'){
	            							console.log( keys[i]+ '= '+res[keys[i]]);
	            							if(!isNaN(res[keys[i]]) && res[keys[i]] > 1){
	            								
            									this.track[keys[i]] = res[keys[i]]; //reset recorded set
	            							}else{
	            								
	            								if(Array.isArray(res[keys[i]])){
	            									let sudothis = this;
	            									res[keys[i]].map(function(r,ind){
	            										sudothis.removeFromTracker(keys[i],r);
	            										return r;
	            									});
	            								}else{
	            									console.log('track item set to 0'+'->'+this.track[keys[i]]);
	            									//Ensure Data is not corrupted by Tracker
	            									if(Array.isArray(this.track[keys[i]])){
	            										this.track[keys[i]] = [];
	            										console.log('Array.isArray track item set to 0'+'->'+this.track[keys[i]]);
	            									}else if(typeof res[keys[i]] === 'object'){
	            										let rk = Object.keys(res[keys[i]]);
	            										let sudothis = this;
	            										rk.map(function(r,ind){
		            										sudothis.removeFromTracker(keys[i],r);
		            										return r;
		            									});
	            									}else{
	            										this.track[keys[i]] = 0; //reset recorded set	
	            									}
	            								}
	            							}
	            						}

	            					}
	            				}
	            				if(keys[i] == 'updates'){
	            					this.storage.get('updates').then(storedupdates=>{
	            						if(!storedupdates){storedupdates=[];}
	            						if(res[keys[i]] && res[keys[i]].length){
	            							for(let k=0;k<res[keys[i]].length;k++){
	            								if(!this.matchObj(storedupdates,res[keys[i]][k])){
	            									storedupdates.push(res[keys[i]][k]);
	            								}
		            						}
		            						this.storage.set('updates',storedupdates);

	            						}
	            					});
	            				}
	            			}
	            			console.log(this.track);
	            			this.storage.set('track',this.track);
	            			this.storage.set('lastccess',this.timestamp);
	            			
	            		}

        			}
        			this.storage.get('updates').then(storedupdates=>{	
						if(storedupdates && storedupdates.length){
							this.unread_notifications_count = storedupdates.length;
							this.storage.get('readupdates').then(readupdates=>{
								if(readupdates){
						            this.unread_notifications_count = storedupdates.length-readupdates.length;
						        }
							});
							console.log('unread_notifications_count in config -> '+this.unread_notifications_count);
						}
					});
            	}
            });
		}else{
			console.log('TRACKING');
			
			var url = `${this.baseUrl}track/?access=`+this.lastaccess;

			if(!this.settings.client_secret.length){
				url = `${this.baseUrl}track/?client_id=`+this.settings.client_id;
			}


			this.http.get(url)
            .map(response =>{ return response.json();
            }).subscribe(res=>{
            	console.log('GET SITE TRACKER');
            	if(res){
            		if(res.version != this.track.version){
            			//Re-record all cached data.
            			this.defaultTrack.version  = res.version;
            			this.track = this.defaultTrack;

            		}else{

            			if(res.counter != this.track.counter || !res.counter){
	            			var keys = Object.keys(res);
	            			
	            			if(keys.length){
		            			for(let i=0;i<keys.length;i++){
		            				if(keys[i] in this.track){
		            					if(Array.isArray(this.track[keys[i]]) && Array.isArray( res[keys[i]])){
		            						let missing = this.track[keys[i]].filter(item => res[keys[i]].indexOf(item) < 0);
		            						this.track[keys[i]] = missing;
		            					}else{

		            						if(res[keys[i]] && keys[i] != 'version'){
		            							if(!isNaN(res[keys[i]]) && res[keys[i]] > 1){
	            									this.track[keys[i]] = res[keys[i]]; //reset recorded set
		            							}else{
		            								if(Array.isArray(res[keys[i]])){
		            									let sudothis = this;
		            									res[keys[i]].map(function(r,ind){
		            										sudothis.removeFromTracker(keys[i],r);
		            										return r;
		            									});
		            								}else{
		            									console.log('track item set to 0'+'->'+this.track[keys[i]]);
		            									//Ensure Data is not corrupted by Tracker
		            									if(Array.isArray(this.track[keys[i]])){
		            										this.track[keys[i]] = [];
		            										console.log('Array.isArray track item set to 0'+'->'+this.track[keys[i]]);
		            									}else if(typeof res[keys[i]] === 'object'){
		            										let rk = Object.keys(res[keys[i]]);
		            										let sudothis = this;
		            										rk.map(function(r,ind){
			            										sudothis.removeFromTracker(keys[i],r);
			            										return r;
			            									});
		            									}else{
		            										this.track[keys[i]] = 0; //reset recorded set	
		            									}
		            								}
		            							}
		            							
		            						}
		            					}
		            				}
		            				if(keys[i] == 'updates'){
		            					console.log('updates key');
		            					this.storage.get('updates').then(storedupdates=>{
		            						if(!storedupdates){storedupdates=[];}

            								for(let k=0;k<res[keys[i]].length;k++){
	            								if(!this.matchObj(storedupdates,res[keys[i]][k])){
	            									storedupdates.push(res[keys[i]][k]);
	            								}
		            						}
		            						this.storage.set('updates',storedupdates);
		            					});
		            				}
		            			}
		            			this.storage.set('track',this.track);
		            			this.storage.set('lastccess',this.timestamp);
		            		}
		            	}
        			}
            		
            		if('client_secret' in res){
            			console.log('Fetching client_secret');
            			this.settings.client_secret = res.client_secret;
            			this.settings.state = res.state;
            			this.save_settings();
            		}
            		
            		
	            		this.storage.get('updates').then(storedupdates=>{		            			
							if(storedupdates){
								this.unread_notifications_count = storedupdates.length;
								this.storage.get('readupdates').then(readupdates=>{
									if(readupdates){
							            this.unread_notifications_count = storedupdates.length-readupdates.length;
							        }
								});
							}
						});
					
            	}
            });
                    
		}
	}
	isString(val) { return typeof val === 'string'; }
	isArray(obj: any): boolean {return Array.isArray(obj);}
}
