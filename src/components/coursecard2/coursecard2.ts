import { Component, Input, OnInit } from '@angular/core';
import { ConfigService } from '../../services/config';
//import { CoursePage } from '../../pages/course/course';
//import { WishlistService } from '../../services/wishlist';

import { BlogPage } from '../../pages/blog/blog';
import { PostPage } from '../../pages/post/post';

@Component({
  selector: 'coursecard2',
  templateUrl: 'coursecard2.html'
})
export class Coursecard2 implements OnInit{

    blogPage= BlogPage;
	postPage= PostPage;
    active:string='';
    @Input('blog') blog;

    constructor(
    	private config:ConfigService) {
    }

    ngOnInit(){
    }
}
