import { Component, Input, OnInit } from '@angular/core';
import { ConfigService } from '../../services/config';
import { CoursePage } from '../../pages/course/course';
import { WishlistService } from '../../services/wishlist';

@Component({
  selector: 'coursecard',
  templateUrl: 'coursecard.html'
})
export class Coursecard implements OnInit{

    coursePage= CoursePage;
    active:string='';
    @Input('course') course;

    constructor(
    	private wishlistService:WishlistService,private config:ConfigService) {
    }

    ngOnInit(){
    	if(this.wishlistService.checkInWishList(this.course)){
    		
    		this.active = 'active';
    	}else{
    		this.active = '';
    	}
    }
	
	getSlashedPrice(){
		//let get_price:any =this.fullCourse.course.price_html[0];
		let pos1 = this.course.price_html[0].html.search('<del>');
		let pos2 = this.course.price_html[0].html.search('</del>');
		if (pos1 >=0) {
			return this.course.price_html[0].html.substr(pos1,pos2-pos1+6);
		} else
		{
			return null;
		}
	}
	
	getPrice(){
		//let get_price:any =this.fullCourse.course.price_html[0];
		let pos1 = this.course.price_html[0].html.search('<ins>');
		let pos2 = this.course.price_html[0].html.search('</ins>');
		if (pos1 >=0) {
			return this.course.price_html[0].html.substr(pos1+5,pos2-pos1+5);
		} else
		{
			return this.course.price_html[0].html;
		}
	}
	
	/*
	getSlashedPrice(){
		let pos1 = this.course.price_html[0].html.search('<del>');
		let pos2 = this.course.price_html[0].html.search('</del>') + 5;
		return this.course.price_html[0].html.substr(pos1,pos2);
	}
	
	getPrice(){
		let pos1 = this.course.price_html[0].html.search('<ins>');
		let pos2 = this.course.price_html[0].html.search('</ins>') + 5;
		return this.course.price_html[0].html.substr(pos1,pos2);
		
	}
	*/
    addToWishlist(){
    	console.log('clicked -'+this.course.name);
    	if(this.active == 'active'){
    		this.wishlistService.removeFromWishList(this.course);
    		this.active='';
    	}else{
    		this.wishlistService.addToWishlist(this.course);
    		this.active='active';
    	}
    }


}
